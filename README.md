# Bollettino Generator v0.1

### Description

Tool utile alla generazione dei bollettini

### Execution

1. Chiedere ad Aldo Lushkja per esecuzione e prima configurazione configurazione

### TODO

1. Impostare l'acquisizione dei parametri da riga di comando: python obtainBollettinoPDF.py -t <token> -u <username> -ip <client-ip> -id <bolletinoID>
2. Dockerizzare il progetto 
3. Refactoring strutturale

### Build Docker Image

1. Spostarsi nella cartella del progetto
2. digitare: docker build -t <nome_immagine_da_creare> .
3. eseguire l'immagine creata con il comando: docker run <nome_immagine>