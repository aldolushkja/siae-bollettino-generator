# Obtain pae token
import sys, getopt
import requests
import xmltodict


def main(argv):
    user = 'gaetano.licciardi.94'
    password = 'password'

    try:
        opts, args = getopt.getopt(argv, "hu:p:", ["user=", "password="])
    except getopt.GetoptError:
        print('test.py -u <username> -p <password>')
        print('using default username')
        # getToken(user, password)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('test.py -u <username> -p <password>')
            sys.exit()
        elif opt in ("-u", "--user"):
            user = arg
        elif opt in ("-p", "--password"):
            password = arg
    #
    # print('User ', user)
    # print('Password ', password)

    # getDocument(getToken(user, password), user, '4000')
    # (token, user, ip, id)
    getDocument('de800e37-3ff6-4486-b6d3-3b25b19784cd', 'gianluca.verrengia.08', '2.228.32.65', '697084')


#
# def getDati(id):
#     server = 'SQL_PortaleAssociati.test.siae'
#     database = 'PortaleAssociati'
#     username = 'spas_admin'
#     password = 'spas_admin'
#     driver = '{ODBC Driver 17 for SQL Server}'
#     cnxn = pyodbc.connect(
#         'DRIVER=' + driver + ';SERVER=' + server + ';PORT=1433;DATABASE=' + database + ';UID=' + username + ';PWD=' + password)
#     cursor = cnxn.cursor()
#     cursor.execute("SELECT * from PORTALE_BOLLETTINO_MUSICA WHERE id = id")
#     # cursor.execute(
#     # "SELECT * FROM PORTALE_BOLLETTINO_MUSICA AS pbm JOIN PORTALE_BOLLETTINO_ALLEGATI AS pba ON pbm.ID = pba.BOLLETTINO_ID WHERE BOLLETTINO_ID = 4073")
#     row = cursor.fetchone()
#     print(str(row[0]))
#     id = row[0]
#     return id


def getToken(user, password):
    token = 'invalidToken'

    # token SSO generation
    url = "http://ssows.test.siae/AuthenticationService.asmx?wsdl"
    headers = {'Content-Type': 'text/xml'}
    body = """
			<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sso="http://SSO.siae.it/">
			   <soapenv:Header/>
			   <soapenv:Body>
			      <sso:Login>
			         <!--Optional:-->
			         <sso:userName>""" + user + """</sso:userName>
			         <!--Optional:-->
			         <sso:password>""" + password + """</sso:password>
			         <!--Optional:-->
			         <sso:application>PAE</sso:application>
			         <!--Optional:-->
			         <sso:ip>?</sso:ip>
			      </sso:Login>
			   </soapenv:Body>
			</soapenv:Envelope>

	"""
    response = requests.post(url, data=body, headers=headers)
    responseXml = xmltodict.parse(response.content)

    # Scrive un file.
    out_file = open("1 - LoginSSO.txt", "w")
    out_file.write(str(responseXml))
    out_file.close()

    token = responseXml['soap:Envelope']['soap:Body']['LoginResponse']['LoginResult']['Token']

    # print(token)
    # record generation PORTALE_SESSIONI
    url = "http://paeService.test.siae/PaeLoginService/LoginService?wsdl"
    headers = {'Content-Type': 'text/xml'}
    body = """
			<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ser='http://server.login.portale.siae.it/'>
			  <soapenv:Header />
			  <soapenv:Body>
			    <ser:login>
			      <header>
			        <CodiceProcesso>PAE01</CodiceProcesso>
			        <Mittente>Portale WEB</Mittente>
			        <Destinatario>PAE</Destinatario>
			        <Messaggi>
			          <item>
			            <ID>MU15536608821360</ID>
			            <Riferimento>1</Riferimento>
			            <TimeStamp>2019-03-26T10:06:04</TimeStamp>
			          </item>
			        </Messaggi>
			        <TipoElaborazione>S</TipoElaborazione>
			        <UserName>""" + user + """</UserName>
			      </header>
			      <requestData>
			        <token>""" + token + """</token>
			        <locale>IT</locale>
			        <fields>
			          <fieldsRequest>
			            <name>provenienza</name>
			            <value>INTERNET</value>
			          </fieldsRequest>
			        </fields>
			      </requestData>
			    </ser:login>
			  </soapenv:Body>
			</soapenv:Envelope>

	"""
    response = requests.post(url, data=body, headers=headers)
    responseXml = xmltodict.parse(response.content)
    # Scrive un file.
    out_file = open("2 - LoginPAE.txt", "w")
    out_file.write(str(responseXml))
    out_file.close()
    # token activation -> PaeLogin
    url = "http://paeService.test.siae/PaeLoginService/CambiaPosizioneService?wsdl"
    headers = {'Content-Type': 'text/xml'}
    body = """
			<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://server.login.portale.siae.it/">
			  <soapenv:Header/>
			  <soapenv:Body>
			    <ser:paeLogin>
			      <header>
			        <CodiceProcesso>PAE02B</CodiceProcesso>
			        <Mittente>Portale WEB</Mittente>
			        <Destinatario>PAE</Destinatario>
			        <Messaggi>
			          <item>
			            <ID>MU15224698054170</ID>
			            <Riferimento>1</Riferimento>
			            <TimeStamp>2018-03-30T09:27:31</TimeStamp>
			          </item>
			        </Messaggi>
			        <TipoElaborazione>S</TipoElaborazione>
			        <UserName>""" + user + """</UserName>
			      </header>
			      <requestData>
			        <token>""" + token + """</token>
			        <ipClient>127.0.0.1</ipClient>
			        <locale>IT</locale>
			        <fields>
			          <fieldsRequest>
			            <name>posizione</name>
			            <value>9419-0</value>
			          </fieldsRequest>
			        </fields>
			        <iscrivendo/>
			        <cognome>UNIVERSAL MUSIC PUBLISHING RICORDI SRL</cognome>
			        <nome/>
			        <idCategoria/>
			        <categoria>2</categoria>
			        <qualifica>E</qualifica>
			        <tipoAppartenenza>ASSOCIATO</tipoAppartenenza>
			        <nomeArte/>
			        <titoloAccademico/>
			        <detto/>
			        <codiceCAE>547691219</codiceCAE>
			        <societaStraniera/>
			        <sezioniCRM>
			          <sezioneCRM>
			            <codice>1</codice>
			            <id>1</id>
			            <sezione>LIRICA</sezione>
			          </sezioneCRM>
			          <sezioneCRM>
			            <codice>2</codice>
			            <id>2</id>
			            <sezione>MUSICA</sezione>
			          </sezioneCRM>
			          <sezioneCRM>
			            <codice>3</codice>
			            <id>3</id>
			            <sezione>DOR</sezione>
			          </sezioneCRM>
			          <sezioneCRM>
			            <codice>4</codice>
			            <id>4</id>
			            <sezione>OLAF</sezione>
			          </sezioneCRM>
			        </sezioniCRM>
			      </requestData>
			    </ser:paeLogin>
			  </soapenv:Body>
			</soapenv:Envelope>
	"""
    response = requests.post(url, data=body, headers=headers)
    responseXml = xmltodict.parse(response.content)
    # Scrive un file.
    out_file = open("3 - CambioPosizionePAE.txt", "w")
    out_file.write(str(responseXml))
    out_file.close()
    print("Generated Token: ", token)

    return token


def getDocument(token, user, ip, idDoc):
    #  generaRicevutaPDF Request -> blob
    url = "http://paeservice.servizi.siae/PaeBollettinoMusicaService/BollettinoMusicaService?wsdl"
    headers = {'Content-Type': 'text/xml', 'SoapAction': 'generazionePDFBollettino'}
    body = """
    <soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ser='http://server.bollettino.pae.siae.it/'>
  <soapenv:Header />
  <soapenv:Body>
    <ser:bollettinoRequest>
      <header>
        <CodiceProcesso>PAE36</CodiceProcesso>
        <Mittente>Portale WEB</Mittente>
        <Destinatario>PAE</Destinatario>
        <Messaggi>
          <item>
            <ID>MU15585579929120</ID>
            <Riferimento>1</Riferimento>
            <TimeStamp>2019-05-22T15:30:51</TimeStamp>
          </item>
        </Messaggi>
        <TipoElaborazione>S</TipoElaborazione>
        <UserName>""" + user + """</UserName>
      </header>
      <requestData>
        <token>""" + token + """</token>
        <ipClient>""" + ip + """</ipClient>
        <locale>IT</locale>
        <fields>
          <fieldsRequest>
            <name>idBollettino</name>
            <value>""" + idDoc + """</value>
          </fieldsRequest>
        </fields>
<DatiBollettinoMusica>
	<altriTitoli/>
	<aventiDiritto>
		<aventiDiritto>
			<DEM>siae</DEM>
			<DRM>siae</DRM>
			<categoria>AUTORE</categoria>
			<cognome>VERRENGIA</cognome>
			<compilatore>true</compilatore>
			<flagFirmaElettronicaAttivabile>Y</flagFirmaElettronicaAttivabile>
			<flagFirmaElettronicaValidita>Y</flagFirmaElettronicaValidita>
			<idcrm>3217384</idcrm>
			<ipiNumber>198766104</ipiNumber>
			<mail>verrengiamusic@hotmail.com</mail>
			<nome>GIANLUCA</nome>
			<nominativo>VERRENGIA GIANLUCA</nominativo>
			<nuovo>false</nuovo>
			<posizioneSiae>123805</posizioneSiae>
			<qualifica>CO</qualifica>
			<quotaDEM>12</quotaDEM>
			<quotaDRM>50</quotaDRM>
			<tipologiaIscrizione>X</tipologiaIscrizione>
		</aventiDiritto>
		<aventiDiritto>
			<DEM>siae</DEM>
			<DRM>siae</DRM>
			<categoria>EDITORE</categoria>
			<cognome>GAP MEDIA SRL</cognome>
			<compilatore>false</compilatore>
			<flagFirmaElettronicaAttivabile>N</flagFirmaElettronicaAttivabile>
			<flagFirmaElettronicaValidita>N</flagFirmaElettronicaValidita>
			<ipiNumber>889720772</ipiNumber>
			<nominativo>GAP MEDIA SRL</nominativo>
			<nuovo>true</nuovo>
			<posizioneSiae>268455</posizioneSiae>
			<qualifica>EO</qualifica>
			<quotaDEM>12</quotaDEM>
			<quotaDRM>50</quotaDRM>
			<sottoConto>04</sottoConto>
			<tipoInserimento>R</tipoInserimento>
			<tipologiaIscrizione>X</tipologiaIscrizione>
		</aventiDiritto>
	</aventiDiritto>
	<colonnaSonora>N</colonnaSonora>
	<durataMinuti>03</durataMinuti>
	<durataOre>00</durataOre>
	<durataSecondi>00</durataSecondi>
	<genere>21</genere>
	<linguaTitolo>IT</linguaTitolo>
	<modalita>BP</modalita>
	<musica>O</musica>
	<posizioneSis>123805</posizioneSis>
	<scalaQuoteDEM>24</scalaQuoteDEM>
	<sostituzionePrecedente>N</sostituzionePrecedente>
	<territori>
		<territori>
			<operazione>WL-</operazione>
			<territorio>Mondo</territorio>
		</territori>
	</territori>
	<testo>N</testo>
	<tipoOpera>E</tipoOpera>
	<tipologiaBrano>0</tipologiaBrano>
	<titoliBraniStaccati/>
	<titolo>CARS IN THE SKY</titolo>
	<titoloOperaAudioVisiva>TORQUE</titoloOperaAudioVisiva>
</DatiBollettinoMusica>
      </requestData>
    </ser:bollettinoRequest>
  </soapenv:Body>
</soapenv:Envelope>
        """
    response = requests.post(url, data=body, headers=headers)
    responseXml = xmltodict.parse(response.content)

    # Scrive un file.
    out_file = open("4 - GenerazionePDFBollettino.txt", "w")
    out_file.write(str(responseXml))
    out_file.close()
    # responseDict = dict(responseXml)

    blob = \
        responseXml['soap:Envelope']['soap:Body']['ns2:BollettinoResponsePDF']['BollettinoResponseDTO']['responseData'][
            'blob']
    # print(">>>>>>>>>> Response: ", responseXml)

    # Scrive un file.
    out_file = open("5 - Blob.txt", "w")
    out_file.write(str(responseXml))
    out_file.close()

    url = "http://paeservice.servizi.siae/PaeFileService/FileService?wsdl"
    headers = {'Content-Type': 'text/xml', 'SoapAction': 'uploadBollettinoAllegati'}
    body = """
    <soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ser='http://server.file.pae.siae.it/'>
  <soapenv:Header />
  <soapenv:Body>
    <ser:fileRequestUpload>
      <header>
        <CodiceProcesso>PAE17</CodiceProcesso>
        <Mittente>Portale WEB</Mittente>
        <Destinatario>PAE</Destinatario>
        <Messaggi>
          <item>
            <ID>MU15585935709630</ID>
            <Riferimento>1</Riferimento>
            <TimeStamp>2019-05-22T15:30:53</TimeStamp>
          </item>
        </Messaggi>
        <TipoElaborazione>S</TipoElaborazione>
        <UserName>""" + user + """</UserName>
      </header>
      <requestData>
        <token>""" + token + """</token>
        <ipClient>""" + ip + """</ipClient>
        <locale>IT</locale>
        <fields>
          <fieldsRequest>
            <name>tipoAllegato</name>
            <value>deposito</value>
          </fieldsRequest>
          <fieldsRequest>
            <name>idBollettino</name>
            <value>""" + idDoc + """</value>
          </fieldsRequest>
        </fields>
        <fileName>DocumentoFirmato_Digitalmente_""" + idDoc + """.pdf</fileName>
        <blob>""" + blob + """</blob>
          </requestData>
        </ser:fileRequestUpload>
      </soapenv:Body>
    </soapenv:Envelope>
        """

    response = requests.post(url, data=body, headers=headers)
    responseXml = xmltodict.parse(response.content)

    # Scrive un file.
    out_file = open("6 - UploadBollettino.txt", "w")
    out_file.write(str(body))
    out_file.close()

    # print(">>>> Response upload: ", response)


if __name__ == "__main__":
    main(sys.argv[1:])
